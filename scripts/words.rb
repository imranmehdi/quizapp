require 'json'

categories = %w(
  verbs
  time
  people
  office
  numbers
  nature
  health
  food
  feelings
  clothes
  body
  animals
)

categories_hash = {}

categories.each do |category|
  File.open("./rawData/#{category}.csv", encoding: 'bom|utf-8') do |f|
    words_array = []

    f.each_line do |line|
      line.strip!
      german = line.split(';')[0]
      urdu = line.split(';')[1]
      english = line.split(';')[2]
      words_array << { 'word' => german.to_s, 'ur' => urdu.to_s, 'en' => english.to_s }

      # all words sorted alphabetically with translations
      words_array.sort! { |a, b| a['word'] <=> b['word'] }
      words_array.sort! { |a, b| a['translation'].to_i <=> b['translation'].to_i } if category == 'numbers'

      categories_hash[category] = words_array
    end
  end
end

File.open('../src/assets/words/words.json', 'w') { |file| file.write(categories_hash.to_json) }

# craete Quiz data
german_words = []
urdu_words = []
english_words = []
urdu_questions = []
english_questions = []
hash = {}

categories.each do |category|
  categories_hash[category].each do |words|
    german_words << words['word']
    urdu_words << words['ur']
    english_words << words['en']
  end
end

german_words.each_with_index do |word, index|
  # get 3 random integers between 0 and number of words, avoid duplicate
  rndm_indices = (0..german_words.size).to_a.sample 3 # example: [3, 62, 23]

  if rndm_indices.include?(index)
    rndm_indices[rndm_indices.find_index(index)] = index + 1
  end

  if rndm_indices.include?(index)
    p rndm_indices
    p index
    p ''
  end

  question_object = {
    'question' => word,
    'options' => [
      urdu_words[index],
      urdu_words[rndm_indices[0]],
      urdu_words[rndm_indices[1]],
      urdu_words[rndm_indices[2]]
    ],
    'answer' => urdu_words[index]
  }
  question_object['options'].shuffle!
  urdu_questions << question_object

  question_object = {
    'question' => word,
    'options' => [
      english_words[index],
      english_words[rndm_indices[0]],
      english_words[rndm_indices[1]],
      english_words[rndm_indices[2]]
    ],
    'answer' => english_words[index]
  }
  question_object['options'].shuffle!
  english_questions << question_object
end
hash['urdu'] = urdu_questions
hash['english'] = english_questions

File.open('../src/assets/words/quizData.json', 'w') { |file| file.write(hash.to_json) }
