import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { DatabaseService } from '../services/database/database.service';
import { AuthenticationService } from '../services/user/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.page.html',
  styleUrls: ['./statistics.page.scss'],
})
export class StatisticsPage implements OnInit {
  @Input() score: number;
  @Input() displayName: string;
  @Input() avatar: string;
  globalScores$: Observable<any>;
  score$: Observable<any>;

  constructor(
    private databaseService: DatabaseService,
    private authService: AuthenticationService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.globalScores$ = this.databaseService.getUsers$();
    this.score$ = this.databaseService.getUser$(this.authService.getFirebaseUser().uid);
  }

  goToHomePage() {
    this.router.navigate(['/home']);
  }
}
