import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss'],
})
export class OptionsComponent implements OnInit {
  @Input() options: String[];
  @Input() answer: string;
  @Input() lockOptions: boolean;
  @Input() selectedLanguage: string;

  @Output() selectedOptionEmitter = new EventEmitter();

  constructor() { }

  ngOnInit() { }

  getSelectedOption(option: string) {
    this.lockOptions = true;
    this.selectedOptionEmitter.emit({selected: option, answer: this.answer});
  }
}
