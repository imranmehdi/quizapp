import { transition, trigger, useAnimation } from '@angular/animations';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { tada } from 'ng-animate';

@Component({
  selector: 'app-option',
  templateUrl: './option.component.html',
  styleUrls: ['./option.component.scss'],
  animations: [
    trigger('tada', [
      transition('* => *', useAnimation(tada, {params: { timing: 1, delay: 0 }}))
    ])
  ],
})
export class OptionComponent implements OnInit {
  color = 'light';
  @Input() option: string;
  @Input() answer: string;
  @Input() lockOptions: boolean;
  @Input() selectedLanguage: string;

  @Output() selectedOptionEmitter = new EventEmitter();

  constructor() {}

  ngOnInit() {
  }

  onSelect() {
    this.option === this.answer ? this.color = 'success' : this.color = 'danger';
    this.selectedOptionEmitter.emit(this.option);
  }
}
