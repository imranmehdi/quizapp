import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss'],
})
export class TimerComponent implements OnInit, OnDestroy {
  intervalId = 0;
  seconds = 20;
  timeOut = false;
  @Output() timeOutEventEmitter = new EventEmitter();

  clearTimer() {
    clearInterval(this.intervalId);
  }

  ngOnInit() {
    this.start();
  }

  ngOnDestroy() {
    this.clearTimer();
  }

  start() {
    this.seconds = 20;
    this.countDown();
  }

  stop() {
    this.clearTimer();
  }

  private countDown() {
    this.clearTimer();
    this.intervalId = window.setInterval(() => {
      this.seconds -= 1;
      if (this.seconds === 0) {
        this.timeOut = true;
        this.stop();
        this.timeOutEventEmitter.emit(this.timeOut);
      }
    }, 1000);
  }
}
