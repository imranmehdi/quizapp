import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GamePage } from './game.page';
import { QuestionCardComponent } from './question-card/question-card.component';
import { OptionsComponent } from './options/options.component';
import { OptionComponent } from './options/option/option.component';
import { PlayerInfoComponent } from './player-info/player-info.component';
import { StatisticsCardComponent } from './statistics-card/statistics-card.component';
import { TimerComponent } from './timer/timer.component';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: GamePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [
    GamePage,
    QuestionCardComponent,
    OptionsComponent,
    OptionComponent,
    PlayerInfoComponent,
    StatisticsCardComponent,
    TimerComponent
  ]
})
export class GamePageModule {}
