import { transition, trigger, useAnimation } from '@angular/animations';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { jackInTheBox } from 'ng-animate';
import { Question } from '../../models/question';


@Component({
  selector: 'app-question-card',
  templateUrl: './question-card.component.html',
  styleUrls: ['./question-card.component.scss'],
  animations: [
    trigger('jackInTheBox', [
      transition('* => *', useAnimation(jackInTheBox, {params: { timing: 1, delay: 0 }}))
    ])
  ],
})
export class QuestionCardComponent implements OnInit, OnChanges {
  @Input() question: Question;
  jackInTheBox = 'inactive';

  constructor() { }

  ngOnInit() {}

  ngOnChanges() {
    this.startAnimation();
  }

  startAnimation() {
    this.jackInTheBox = this.jackInTheBox === 'inactive' ? 'active' : 'inactive';
  }
}
