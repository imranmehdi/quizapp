import { Component, OnInit, Input, Output, EventEmitter, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { TimerComponent } from '../timer/timer.component';

@Component({
  selector: 'app-player-info',
  templateUrl: './player-info.component.html',
  styleUrls: ['./player-info.component.scss'],
})
export class PlayerInfoComponent implements OnInit, OnChanges {
  @ViewChild(TimerComponent)
  private timerComponent: TimerComponent;

  @Input() score: number;
  @Input() round: number;
  @Input() lockOptions: boolean;
  @Output() timeOutEventEmitter = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  getTimeOutEvent(timeOut: boolean) {
    this.timeOutEventEmitter.emit(timeOut);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.lockOptions) {
      if (this.lockOptions === true) {
        this.stopTimer();
      }
      if (this.lockOptions === false) {
        this.startTimer();
      }
    }
  }

  startTimer() {
    this.timerComponent.start();
 }

 stopTimer() {
  this.timerComponent.stop();
}
}
