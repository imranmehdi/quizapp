import { Component, Input, OnInit, ViewChild } from '@angular/core';
import * as Chart from 'chart.js';
import { Observable } from 'rxjs';
import { DatabaseService } from '../../services/database/database.service';

const totalScore = 10;

@Component({
  selector: 'app-statistics-card',
  templateUrl: './statistics-card.component.html',
  styleUrls: ['./statistics-card.component.scss'],
})
export class StatisticsCardComponent implements OnInit {
  @Input() score: number;
  @Input() displayName: string;
  @Input() avatar: string;
  doughnutChart: any;
  @ViewChild('doughnutCanvas') doughnutCanvas;

  globalScores$: Observable<any>;

  constructor(private databaseService: DatabaseService) { }

  ngOnInit() {
    this.doghnutChart();
    this.globalScores$ = this.databaseService.getUsers$();
  }

  doghnutChart() {
    const data = {
      labels: ['Green', 'Red'],
      datasets: [
        {
          data: [this.score, totalScore - this.score],
          backgroundColor: ['#0CC44C', '#D32027'],
          hoverBackgroundColor: ['#0CC44C', '#D32027']
        }
      ]
    };

    const promisedDeliveryChart = new Chart(this.doughnutCanvas.nativeElement, {
      type: 'doughnut',
      data: data,
      options: {
        responsive: true,
        legend: {
          display: false
        }
      }
    });

    // const scoreText = (100 / totalScore * this.score) + '%';

    // Chart.pluginService.register({
    //   beforeDraw: function(chart) {
    //     const width = chart.chartArea.right - chart.chartArea.left,
    //           height = chart.chartArea.bottom - chart.chartArea.top,
    //           ctx = chart.ctx;

    //     ctx.restore();
    //     const fontSize = (height / 114).toFixed(2);
    //     ctx.font = fontSize + 'em sans-serif';
    //     ctx.textBaseline = 'middle';

    //     const text = scoreText;
    //     const textX = Math.round((width - ctx.measureText(text).width) / 2),
    //           textY = height / 2;

    //     ctx.fillText('', textX, textY);
    //     ctx.fillText(text, textX, textY);
    //     ctx.save();
    //   }
    // });
  }
}
