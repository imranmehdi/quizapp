import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { transition, trigger, useAnimation } from '@angular/animations';
import { zoomIn } from 'ng-animate';
import { Observable } from 'rxjs';
import { WordsService } from '../services/words/words.service';
import { Question } from '../models/question';
import { SelectedOption } from '../models/option';
import { DatabaseService } from '../services/database/database.service';
import { AuthenticationService } from '../services/user/authentication.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.page.html',
  styleUrls: ['./game.page.scss'],
  animations: [
    trigger('zoomIn', [
      transition('* => *', useAnimation(zoomIn, {params: { timing: 2, delay: 0 }}))
    ])
  ],
})
export class GamePage implements OnInit {
  user: any;
  gameStarted: boolean;
  lockOptions: boolean;
  currentQuestionIndex: number;
  round: number;
  score: number;
  selectedLanguage = 'urdu';

  questions$: Observable<Question[]>;

  constructor(
    private wordsService: WordsService,
    private databaseService: DatabaseService,
    private authService: AuthenticationService,
    private router: Router
  ) { }

  ngOnInit() {
    const firebaseUser = this.authService.getFirebaseUser();
    this.databaseService.getUser$(firebaseUser.uid).subscribe(
      data => this.user = data
    );
    this.initGame();
  }

  initGame() {
    this.questions$ = this.wordsService.getQuizData$(this.selectedLanguage);

    this.gameStarted = false;
    this.lockOptions = false;
    this.currentQuestionIndex = 0;
    this.round = 1;
    this.score = 0;
  }

  startGame() {
    this.initGame();
    this.gameStarted = true;
  }

  endGame() {
    this.gameStarted = false;
    this.updateUser();
  }

  updateUser() {
    this.user.score += this.score;
    this.databaseService.updateUser(this.user);
  }

  nextQuestion() {
    this.lockOptions = false;
    this.currentQuestionIndex++;
    this.round++;

    // if 10 questions completed, end game
    if (this.round > 10) { this.endGame(); }
  }

  async getSelectedOptionEvent(option: SelectedOption) {
    this.lockOptions = true;
    this.setScore(option);

    await this.delay(3000);

    this.nextQuestion();
  }

  async getTimeOutEvent(timeOut: boolean) {
    if (timeOut) {
      this.lockOptions = true;

      await this.delay(3000);

      this.nextQuestion();
    }
  }

  setScore(option: SelectedOption) {
    if (option.selected === option.answer) {
      this.score++;
    }
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  goToHomePage() {
    this.router.navigate(['/home']);
  }
}
