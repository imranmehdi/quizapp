import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegistrationPage } from './registration.page';
import { TranslateModule } from '@ngx-translate/core';
import { PrivacyPolicyModal } from '../privacy-policy/privacy-policy-modal';

const routes: Routes = [
  {
    path: '',
    component: RegistrationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [RegistrationPage, PrivacyPolicyModal],
  entryComponents: [PrivacyPolicyModal]
})
export class RegistrationPageModule {}
