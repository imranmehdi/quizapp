import { ToastColorType } from 'src/app/models/toastColorTypes';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/user/authentication.service';
import { ToastService } from '../services/toast/toast.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { PasswordValidator } from '../validators/password.validators';
import { PrivacyPolicyModal } from '../privacy-policy/privacy-policy-modal';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {

  username: string;
  email: string;
  password: string;
  confirmPassword: string;

  validationsFormGroup: FormGroup;
  passwordsFormGroup: FormGroup;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private toastService: ToastService,
    private translateService: TranslateService,
    public formBuilder: FormBuilder,
    private modalController: ModalController
    ) { }

  validationMessages = {
    'username': [
      { type: 'required', message: this.translateService.instant('validations.username.required') },
      { type: 'minlength', message: this.translateService.instant('validations.username.minLength') },
      { type: 'maxlength', message: this.translateService.instant('validations.username.maxLength') }
    ],
    'email': [
      { type: 'required', message: this.translateService.instant('validations.email.required') },
      { type: 'pattern', message: this.translateService.instant('validations.email.pattern') }
    ],
    'password': [
      { type: 'required', message: this.translateService.instant('validations.password.required') },
      { type: 'minlength', message: this.translateService.instant('validations.password.minLength') }
    ],
    'confirmPassword': [
      { type: 'required', message: this.translateService.instant('validations.confirmPassword.required') }
    ],
    'passwordsFormGroup': [
      { type: 'areEqual', message: this.translateService.instant('validations.passwordsFormGroup.areEqual') }
    ]
  };

  ngOnInit() {
    this.passwordsFormGroup = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ])),
      confirmPassword: new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areEqual(formGroup);
    });

    this.validationsFormGroup = this.formBuilder.group({
      username: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(12)
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      passwordsFormGroup: this.passwordsFormGroup
    });
  }

  register() {
    this.authService.registerUser(this.username, this.email, this.password)
     .then(res => {
       const message = this.translateService.instant('registerPage.success');
       this.showToastMessage(message, 'success');
       this.goToLoginPage();
      }, err => {
        const message = this.translateService.instant('registerPage.error');
        this.showToastMessage(message, 'danger');
     });
  }

  goToLoginPage() {
    this.router.navigate(['/login']);
  }

  showToastMessage(message: string, type: ToastColorType) {
    this.toastService.presentToast(message, type);
  }

  async openModal() {
    const modal: HTMLIonModalElement =
      await this.modalController.create({
        component: PrivacyPolicyModal
      });
    await modal.present();
  }
}
