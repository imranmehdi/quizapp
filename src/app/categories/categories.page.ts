import { Component, OnInit } from '@angular/core';
import { Category } from '../models/category';
import { WordsService } from '../services/words/words.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit {
  categories: Category[];

  constructor(private wordsService: WordsService, private router: Router) {
    this.categories = this.wordsService.getCategories$();
  }

  ngOnInit() {}

  goToHomePage() {
    this.router.navigate(['/home']);
  }

  goToSelectedCategory(category: string) {
    this.router.navigate(['/categories/' + category]);
  }
}
