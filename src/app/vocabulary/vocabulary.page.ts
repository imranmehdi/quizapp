import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { WordsService } from '../services/words/words.service';
import { Observable, combineLatest } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FormControl} from '@angular/forms';
import { Word } from '../models/word';

@Component({
  selector: 'app-vocabulary',
  templateUrl: './vocabulary.page.html',
  styleUrls: ['./vocabulary.page.scss'],
})
export class VocabularyPage implements OnInit {

  words$: Observable<Word[]>;
  form: FormControl;
  searchText$: Observable<string>;
  filteredWords$: Observable<Word[]>;
  searchBarActive = false;

  constructor(private wordsService: WordsService, private router: Router) {}

  ngOnInit() {
    this.words$ = this.wordsService.getWords$();
    this.form = new FormControl('');
    this.searchText$ = this.form.valueChanges.pipe(startWith(''));
    this.filteredWords$ = combineLatest(this.words$, this.searchText$).pipe(
      map(([words, filterString]) => words.filter(word => {
        return (word.word.toUpperCase().indexOf(filterString.toUpperCase()) !== -1
          || word.en.toUpperCase().indexOf(filterString.toUpperCase()) !== -1
          || word.ur.indexOf(filterString) !== -1);
      }))
    );
  }

  toggleSearchBar() {
    this.searchBarActive ? this.searchBarActive = false : this.searchBarActive = true;
    this.form.setValue('');
  }

  goToHomePage() {
    this.router.navigate(['/home']);
  }
}
