import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './services/user/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: './login/login.module#LoginPageModule'
  },
  {
    path: 'registration',
    loadChildren: './registration/registration.module#RegistrationPageModule'
  },
  {
    path: 'reset_password',
    loadChildren:
      './reset-password/reset-password.module#ResetPasswordPageModule'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'vocabulary',
    loadChildren: './vocabulary/vocabulary.module#VocabularyPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'categories',
    loadChildren: './categories/categories.module#CategoriesPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'categories/:category',
    loadChildren: './category-vocabulary/category-vocabulary.module#CategoryVocabularyPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'game',
    loadChildren: './game/game.module#GamePageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'settings',
    loadChildren: './settings/settings.module#SettingsPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'statistics',
    loadChildren: './statistics/statistics.module#StatisticsPageModule',
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
