export interface Sound {
  key: string;
  asset: string;
  isNative: boolean;
}
