export interface Question {
  question: String;
  options: String[];
  answer: String;
}
