export interface SelectedOption {
  selected: string;
  answer: string;
}
