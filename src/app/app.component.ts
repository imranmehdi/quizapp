import { Component } from '@angular/core';
import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import * as firebase from 'firebase';
import { firebaseConfig } from '../environments/firebaseConfigs';
import { LanguageService } from './services/language/language.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  rootPage;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private navCtrl: NavController,
    private languageService: LanguageService
    ) {
    this.initializeApp();
  }

  initializeApp() {
    firebase.initializeApp(firebaseConfig);

    this.platform.ready().then(() => {
      this.statusBar.styleDefault();

      if (this.platform.is('android')) {
        this.statusBar.backgroundColorByHexString('#fff');
      }

      this.languageService.setInitialAppLanguage();

      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          this.navCtrl.navigateRoot('/home');
          this.splashScreen.hide();
        } else {
          this.navCtrl.navigateRoot('/login');
          this.splashScreen.hide();
        }
      });
    });
  }
}
