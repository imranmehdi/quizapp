import { ToastColorType } from 'src/app/models/toastColorTypes';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/user/authentication.service';
import { ToastService } from '../services/toast/toast.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email: string;
  password: string;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private toastService: ToastService,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
  }

  login() {
    this.authService.loginUser(this.email, this.password)
    .then(res => {
        const message = this.translateService.instant('loginPage.success');
        this.showToastMessage(message, 'success');
        this.router.navigate(['/home']);
      }, error => {
        const message = this.translateService.instant('loginPage.error');
        this.showToastMessage(message, 'danger');
      }
    );
  }

  goToHomePage() {
    this.router.navigate(['/home']);
  }

  showToastMessage(message: string, type: ToastColorType) {
    this.toastService.presentToast(message, type);
  }
}
