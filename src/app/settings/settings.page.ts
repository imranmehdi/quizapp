import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/user/authentication.service';
import { ToastService } from '../services/toast/toast.service';
import { LanguageService } from '../services/language/language.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastColorType } from '../models/toastColorTypes';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  errorMessage = '';
  selectedLanguage = this.languageService.getSelectedLanguage();
  availableLanguages = this.languageService.getLanguages();

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private toastService: ToastService,
    private languageService: LanguageService,
    private translateService: TranslateService
  ) { }

  ngOnInit() {
  }

  logout() {
    this.authService.logoutUser()
    .then(res => {
      const message = this.translateService.instant('settingsPage.success');
      this.showToastMessage(message, 'success');
      this.goToLoginPage();
    })
    .catch(err => {
      const message = this.translateService.instant('settingsPage.error');
      this.showToastMessage(message, 'danger');
    });
  }

  goToLoginPage() {
    this.router.navigate(['/login']);
  }

  goToHomePage() {
    this.router.navigate(['/home']);
  }

  showToastMessage(message: string, type: ToastColorType) {
    this.toastService.presentToast(message, type);
  }

  onChange(event: any) {
    this.languageService.setLanguage(event.detail.value);
  }
}
