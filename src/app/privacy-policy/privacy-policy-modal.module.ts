import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PrivacyPolicyModal } from './privacy-policy-modal';

const routes: Routes = [
  {
    path: '',
    component: PrivacyPolicyModal
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  exports: [PrivacyPolicyModal],
  declarations: [PrivacyPolicyModal],
  entryComponents: [PrivacyPolicyModal]
})
export class PrivacyPolicyModalModule {}
