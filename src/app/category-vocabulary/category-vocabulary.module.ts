import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CategoryVocabularyPage } from './category-vocabulary.page';

const routes: Routes = [
  {
    path: '',
    component: CategoryVocabularyPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CategoryVocabularyPage]
})
export class CategoryVocabularyPageModule {}
