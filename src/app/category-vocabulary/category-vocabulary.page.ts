import { Component, OnInit, OnDestroy } from '@angular/core';
import { WordsService } from '../services/words/words.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Word } from '../models/word';

@Component({
  selector: 'app-category-vocabulary',
  templateUrl: './category-vocabulary.page.html',
  styleUrls: ['./category-vocabulary.page.scss'],
})
export class CategoryVocabularyPage implements OnInit, OnDestroy {

  selectedCategory: string;
  words$: Observable<Word[]>;
  subscription: Subscription;

  constructor(
    private wordsService: WordsService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.subscription = this.route.params.subscribe(params => this.selectedCategory = params['category']);
    this.words$ = this.wordsService.getWordsByCategory$(this.selectedCategory);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  goBack() {
    this.router.navigate(['/categories']);
  }
}
