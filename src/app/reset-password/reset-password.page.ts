import { ToastColorType } from 'src/app/models/toastColorTypes';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/user/authentication.service';
import { ToastService } from '../services/toast/toast.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {
  email: string;
  emailSent = false;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private toastService: ToastService,
    private translateService: TranslateService
  ) { }

  ngOnInit() {
  }

  resetPassword() {
    if (this.email) {
      this.authService.resetPassword(this.email).then(res => {
        const message = this.translateService.instant('resetPasswordPage.success');
        this.showToastMessage(message, 'success');
        this.goToLoginPage();
      })
      .catch(error => {
        const message = this.translateService.instant('resetPasswordPage.error');
        this.showToastMessage(message, 'danger');
      });
    }
  }

  goToLoginPage() {
    this.router.navigate(['/login']);
  }

  showToastMessage(message: string, type: ToastColorType) {
    this.toastService.presentToast(message, type);
  }
}
