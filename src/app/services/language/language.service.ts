import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

const LNG_KEY = 'SELECTED_LANGUAGE';

export const languageCodes = {
  EN: 'en',
  DE: 'de',
};

export const languages = [
  { value: languageCodes.EN, name: 'English' },
  { value: languageCodes.DE, name: 'German'}
];

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor(
    private translateService: TranslateService,
    private storage: Storage,
  ) { }

  setInitialAppLanguage() {
    this.storage.get(LNG_KEY).then(val => {
      if (this.isLangSupported(val)) {
        this.setLanguage(val);
      } else {
        // default language = en
        this.setLanguage(languageCodes.EN);
      }
    });
  }

  setLanguage(languageCode: string) {
    if (this.isLangSupported(languageCode)) {
      this.translateService.setDefaultLang(languageCode);
      this.storage.set(LNG_KEY, languageCode);
    }
  }

  getLanguages() {
    return languages;
  }

  getSelectedLanguage() {
    return this.translateService.getDefaultLang();
  }

  isLangSupported(languageCode: string): boolean {
    const values = Object.keys(languageCodes).map(key => languageCodes[key]);
    return values.indexOf(languageCode) !== -1;
  }
}
