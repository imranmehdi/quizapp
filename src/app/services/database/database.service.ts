import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { User } from 'src/app/models/user';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  private ref: AngularFirestoreCollection<User>;

  constructor(private fireStore: AngularFirestore) {
    this.ref = this.fireStore.collection('userData', ref => ref.orderBy('score', 'desc'));
  }

  createUser(user: User) {
    this.ref.add(user);
  }

  getUser$(uid: string) {
    return this.ref.doc(uid).valueChanges();
  }

  getUsers$() {
    return this.ref.valueChanges();
  }

  updateUser(user: User) {
    this.ref.doc(user.uid).update(user);
  }

  deleteUser(uid: string) {
    this.ref.doc(uid);
  }
}
