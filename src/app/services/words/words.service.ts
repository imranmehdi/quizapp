import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Word } from '../../models/word';
import { Observable } from 'rxjs';
import { Question } from '../../models/question';
import { Category } from '../../models/category';

@Injectable({
  providedIn: 'root'
})
export class WordsService {
  constructor(private http: HttpClient) {}

  getWords$(): Observable<Word[]> {
    return this.http.get('../assets/words/words.json').pipe(
      map(data => {
        return Object.keys(data).reduce((res, values) => {
          return res.concat(data[values]);
        }, []);
      }),
      map(data => data.sort((a, b) => a.word.localeCompare(b.word)))
    );
  }

  // returns 10 shuffled questions
  getQuizData$(language: string): Observable<Question[]> {
    return this.http.get('../assets/words/quizData.json').pipe(
      map(data => this.shuffleWords(data[language])),
      map(data => data.slice(0, 10))
    );
  }

  getWordsByCategory$(category: string): Observable<Word[]> {
    return this.http.get('../assets/words/words.json').pipe(
      map(data => data[category])
    );
  }

  getCategories$(): Category[] {
    return [
      {'url': 'animals', 'de': 'Tiere', 'ur': 'جانور'},
      {'url': 'body', 'de': 'Körper', 'ur': 'جسم'},
      {'url': 'clothes', 'de': 'Kleidung', 'ur': 'لباس'},
      {'url': 'feelings', 'de': 'Gefühle', 'ur': 'احساسات'},
      {'url': 'food', 'de': 'Nahrungsmittel', 'ur': 'کھانا'},
      {'url': 'health', 'de': 'Gesundheit', 'ur': 'صحت'},
      {'url': 'nature', 'de': 'Natur', 'ur': 'کائنات'},
      {'url': 'office', 'de': 'Büro', 'ur': 'دفتر'},
      {'url': 'people', 'de': 'Menschen', 'ur': 'لوگ'},
      {'url': 'time', 'de': 'Zeit', 'ur': 'وقت'},
      {'url': 'numbers', 'de': 'Zahlen', 'ur': 'گنتی'},
      {'url': 'verbs', 'de': 'Verben', 'ur': 'فعل'}
    ];
  }

  // Fisher and Yates's shuffle algorithm
  shuffleWords(array): [] {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }
}
