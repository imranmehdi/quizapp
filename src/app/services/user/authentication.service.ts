import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { DatabaseService } from '../database/database.service';
import { Md5 } from 'ts-md5/dist/md5';

@Injectable()
export class AuthenticationService {

  constructor(private databaseService: DatabaseService) {}

  registerUser(username: string, email: string, password: string) {
   return new Promise<any>((resolve, reject) => {
     firebase.auth().createUserWithEmailAndPassword(email, password)
     .then(
        res => {
          resolve(res);
          const user = {
            uid: res.user.uid,
            displayName: username,
            avatar: this.createImageHash(email),
            score: 0
          };
          this.databaseService.createUser(user);
        },
        err => reject(err)
      );
   });
  }

  loginUser(email: string, password: string) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL);
      firebase.auth().signInWithEmailAndPassword(email, password)
      .then(
        res => {
          resolve(res);
        },
        err => reject(err)
      );
   });
  }

  logoutUser() {
    return new Promise((resolve, reject) => {
      if (firebase.auth().currentUser) {
        firebase.auth().signOut()
        .then(() => {
          resolve();
        }).catch((error) => {
          reject();
        });
      }
    });
  }

  resetPassword(email: string) {
    return firebase.auth().sendPasswordResetEmail(email);
  }

  getFirebaseUser() {
    return firebase.auth().currentUser;
  }

  private createImageHash(email: string) {
    const url = 'https://www.gravatar.com/avatar/';
    const hash = Md5.hashStr(email);
    const defaultPic = '?default=monsterid';
    const profilePicture = url + hash + defaultPic;
    return profilePicture;
  }
}
