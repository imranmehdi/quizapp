import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { ToastColorType } from 'src/app/models/toastColorTypes';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toastController: ToastController) { }

  async presentToast(message: string, type: ToastColorType) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: type,
      position: 'top'
    });
    toast.present();
  }
}
